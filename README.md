# OpenMP -for- Labs

Let's learn parallel programming using OpenMP!

## Poisson

The Poisson distribution is a discrete probability distribution that expresses the probability of a given number of events occurring in a fixed interval of time or space if these events occur with a known constant rate and independently of the time since the last event

See [Wikipedia](https://en.wikipedia.org/wiki/Poisson_distribution).

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor. Build the application on your computer and run it.
4. Complete the printing information using OpenMP functions (number of processor and threads)
5. Parallelize the code using OpenMP.


Anything missing? Ideas for improvements? Make a pull request.
